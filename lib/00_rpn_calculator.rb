class RPNCalculator

  attr_reader :value

  def initialize
    @stack = []
  end

  def push(num)
    @stack.push(num)
  end

  def express
    @second = @stack.pop
    @first = @stack.pop
  end


  def plus
    error
    express
    @value = @first + @second
    @stack.push(@value)
  end

  def minus
    error
    express
    @value = @first - @second
    @stack.push(@value)
  end

  def divide
    error
    express
    @value = @first.to_f / @second
    @stack.push(@value)
  end

  def times
    error
    express
    @value = @first * @second
    @stack.push(@value)
  end

  def error
    raise "calculator is empty" if @stack.size < 2
  end

  def tokens(str)
    @tokened = str.split(" ").map do |el|
      case el
      when "+"
        :+
      when "-"
        :-
      when "*"
        :*
      when "/"
        :/
      else
        el.to_i
      end
    end
    @tokened
  end

  def evaluate(str)
    tokens(str).each do |el|
      case el
      when Integer
        push(el)
      when Symbol
        operate(el)
      end
    end
    value
  end

def operate(operator_sym)
  case operator_sym
  when :+
    plus
  when :-
    minus
  when :*
    times
  when :/
    divide
  end
end

end
